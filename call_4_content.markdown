# Call for Content

Greetings Hackers!

This is a call for content for the book, **Community Networks for Hacktivists**. 


Since we started working on Mesh Networks in 2015, we have faced multiple phases of stagnation followed by sudden outburst of contributions. Every time we reboot the project, we learn from our previous mistakes, identify the obstacles and rethink the problems from a new perspective. Every time we envision a new future for Community Networks. From my experience as being a part of this project, I have identified the following as critical components of an optimal strategy towards advancing Community Networks.


- Controlled Experimentation
    - Skill building
    - Knowledge acquisition
- Proposal writing for external funding
- Knowledge transfer : Documentation/Sharing
- Social Outreach


From our previous failures I've identified two major obstacles that have haunted us from the beginning of the project.

1. Lack of funding
2. Lack of Explicit Knowledge

We haven't made much headway in both these fronts. For now I will focus on the problem of lack of explicit knowledge. 


### Lack of resources

The domain of Computer networks is an ocean. It is as old as computer science. Not all of us can read through  Kurose and Ross's and Tanenbaum's text books before getting involved in Community Networks.

Mesh Networks is a relatively young field in Computer Networks. There is no definitive text book for mesh networks, as various aspects of it, are still in research phase. There isn't an organized collection of knowledge. **Wireless Network for the Developing World** is a good place to start, but it just serves as an introductory text. 

We need a organized collection of resources on Mesh Networks. 


### Geographical Separation

As we are from different GLUGs/groups, we don't meet very often. Whenever we do meet for an event, we do not get the time to fully express our opinions and vision. This book will serve as a means for expressing your vision of Community Networks. Sharing your dream is as important as dreaming. This will bring hope and clarity to the people who are new to community networks.

### Never reinvent the wheel

The very few blog articles we have written to document our work in Community Networks, mostly share just the results. The details of the experiments, obstacles and failures, are lost in the process. This means the new people getting involved, need to start from scratch and reinvent the wheel. This can be avoided by intensive, detailed documentation focused on beginners, followed by evaluation and integration of documentation from multiple hackers.

### The Curse of diversity

Networking is a multi-layered subject which involves interaction between diverse disciplines of engineering. This means, a computer science student getting involved in the project, has to either learn antenna design or find a Communications engineer with similar interests. Both are incredibly difficult tasks. We are yet to find a way to solve this problem. One approach is to abstract away the underlying communication principles, and provide a detailed, reproducible blueprint of an antenna designed for a particular purpose. This involves intense labour from our antenna experts. Similarly, an electronics student needs all the help he can get from the computer scientists, in compiling and installing router firmware.

### Documentation as a feat in itself

After the final revision, printed copies of the book can be distributed to all the GLUGs. This will enable new people interested in networking, to know more about about our efforts and our vision and strengthens our guild of researchers.


## How can you contribute?

Some of us are more experienced in/exposed to some areas of mesh networks than others. Hence, each of us have a different vision for Community networks. Some of us want to build Digital Library Network for free knowledge sharing; some of us want to bring internet to the rural areas; some of us are just interesting in networking; some of us who are more ambitious, aim to replace the internet with a decentralized community network. This individual bias adds to the rich diversity of our mesh network community.

One chapter is dedicated to each of us. It could be however long or short as you wish. It could just be a tutorial or a manual on how to use a tool. Or it could summarise your overall experience being a part of the community. It could be an overview of mesh networks, covering every layer, or just a focused study of one aspect of mesh networks. It could be a case study of an existing Community Network, like [Freifunk](). It could be written from socio-economic, academic or technological point of view. Whatever it is, it should be a personal statement from the author to the new student who reads the chapter. Make your contribution, be a part of this collaborative effort.


Our hope for this project is to present the big picture to a new audience, by tying together our loose, seemingly unrelated efforts in advancing community networks.


Next time when a noob asks you what mesh network is, you can answer something along the lines of 

> Oh! hey dude.. We wrote a book on it. Check it out at [https://pymeshnet.gitlab.io/book](https://pymeshnet.gitlab.io/book). Peace!