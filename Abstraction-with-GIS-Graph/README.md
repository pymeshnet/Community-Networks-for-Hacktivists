Any communication network will come across link budgeting (especially wireless
communication networks); network planning. Communication is a physical
phenomenon that enables us to exchange abstract information. Thus to work with
the physical layer, to initiate a connection, even to simulate a field deployment
requires an abstraction that ties the wireless/radio systems of communication 
to something that is fundamentally tangible - that arms our understanding based
on our existing understanding. This is where GIS & Graph systems comes into picture, 
which provides us with abstract visualization and interpretation to understand
the network setup, bootstrapping, planning, deployment, maintenance, extending,
simulating....etc.