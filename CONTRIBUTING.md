# How to Contribute?

1. Make sure you are added as developer in the project
2. Create a folder; name it the same as your chapter name
3. Create a README.md file inside the folder; list all the sub-topics
3. Maintain everything related to your chapter (for example, images), inside that folder
4. Any text based format is acceptable - markdown, .txt, etc,.
5. It would be easier and practical to make the edits online through a browser, instead of cloning the repository and pushing changes
6. You are free to choose the content; you can discuss it in the riot group if you wish to do so
7. Post any queries regarding contribution in the riot group, or create an issue here


## Ideas for Contribution

1.  **Meta**
 * Commons Infrastructure & Community Networks for commoners
 * Community-Radio-Mesh Communication Network System - CONSTITUTION
 * EM Spectrum Adjudication, Regulations & Activism
 * Social Auditing
 * Disaster/Delay resilience
 * Techno-Social-Economic-Ecologic-Political info. & Knowledge
 * Security and Privacy in community networks
 * Local first economy thought process
 * Clarity in understanding Centralization, Decentralization, Grassroots/Community, Distribution
 * Clarity of Usage of Hierarchy
 * Evolution of radical protocols that could act on knowledge of Geo/Climate/Society's - state. (:P)
 * BitTorrent for community networks
 * Social Networking in Community Networks
 * Limitations of mesh networks
 * Incentive system for contributors and node ops

2.  **Search / Surveys / Studies**
 * Mesh Networks for protests : Case studies
 * Historical survey of mesh networks
 * Survey on existing mesh networks
 * Case study of Freifunk (and other mesh networks)
 * Survey on open hardware router projects
 * Survey on openwrt-compatible commercial routers
 * Survey on routing protocols designed for mesh networks
 * Comparitive study of OLSR, BABEL, BATMAN
 * CJDNS, hyperboria and /r/darknetplan
 * Existing p2p applications - survey
 * ZeroNet in Mesh Networks
 * Gnunet in Mesh Networks

3.  **Tutorials / Meetup sessions**
 * **awesome-meshnet** - collection of tutorials, articles on mesh networks
 * Introduction to Spectrum Activism
 * Jargons in Mesh Networks
 * Communication Theory Basics (Analog, Digital Communication;RF Basics)
 * Introduction to FPGA
 * Introduction to SDR
 * Applications in community networks
 * How to hack/reverse-engineer a router?
 * How to contribute to openwrt?
 * How to setup self host services {Information + Communication appliance} (+ self reliance)
 * OpenWrt/LEDE/Gluon Compilation and Flashing guide
 * Create a beginner friendly manual for commandline tools for configuring the wireless settings
 * BitTorrent protocol for beginners

4. **Apps / Services**
 * Voice, Video
 * Messaging, Chatting
 * Resource sharing
 * Wallets
 * Participatory e-Governance
 * Collaborative projects using open data
 * P2P systems (torrenting, mapping)
 * Local Wiki - KIWIX/MediaWiki
 * Administration and Monitoring tools for community networks

5.  **N/W**
 * Geo view, Graph view, N/W status
 * Link budgeting, N/W planning, Stats.
 * Topology study
 * Protocols (Layers & Routing stuff)
 * Experimentation/Test bench - theoretical setups beforehand testing & practical testing
 * Services supported by NW ( like zeroconf -- am i right here??)
 * Overview of the components of a Router - Mesh Node
 * Decentralized DNS paradox

6.  **S/W**
 * Installation
 * Configuration
 * Services Management
 * Security Management
 * Monitoring & collecting data for statistics (about the routing, link state only -- not data)

7.  **F/W**
 * Source compilation
 * Kernel/Device driver hacking to add features when necessary in future
 * List of supported hardware

8.  **H/W**
 * PC based Node assembling
 * Router + Radio designing
 * Router purchasing compatible with F/W (don't go for brand new - device-driver & kernel availability will take time)
 * Introduction to Antennas, design (+ design guide), construction & Wave-guiding
 * Antenna/Wave-guide purchasing
 * Knowledge in Communication Systems / Information Theory (Digital Communication)
 * FPGA
 * Software Controlled Radio
 * Software Defined Radio