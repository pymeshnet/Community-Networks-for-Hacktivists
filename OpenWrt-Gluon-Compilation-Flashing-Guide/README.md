## Preface

Routers play an important role in networks in connecting devices and routing information. The firmware that drives it is of great importance as well. The reason being, its the firmware through which we use the device's capabilities. OpenWrt and its derivatives, a small linux distro that helps in extending the capabilities of the router, instead of the stock firmware that keeps the router in a locked state. 

## What will be discussed?

This chapter would focus on building OpenWrt/Gluon and flashing it on to the router. The general flow would be,

* Prerequisites for building from source
* Flashing router with firmware (new & existing)
* Recovery through failsafe mode