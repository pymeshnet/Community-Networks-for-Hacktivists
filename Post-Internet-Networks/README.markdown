# Post Internet Networks

1. Corporate Will and Human Revolution 
2. Dystopian Present - they OWN us 
3. Utopian Dream - Community owned infrastructure, Decentralized Processing 
4. Stages of Growth - Fugue state, Rejuvenation, Optimization 
5. The Fugue state/Present 
    1. Philosophical Principles - Free Network Manifesto 
    2. Radical Hactivism 
    3. Community Networks Architectures for the fugue state 
    4. Proposals - Social Comm Net, LibraryNet, Network Planning Tools 
    5. External Funding - NGOs 
    6. Third World Problems 
    7. What next? 
6. Benevolent Artificial Intelligence as the alternative Internet